import hashlib, string, random


def crypto(value):
    return str(hashlib.sha256(value.encode('utf-8')).hexdigest())[:30]

def crypto_sess(value):
    return str(hashlib.sha256(value.encode('utf-8')).hexdigest())

def generate_session():
    SESSION_LEN = 245
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(SESSION_LEN))
    return crypto_sess(result_str)
