from flask import Flask, request, render_template, make_response, redirect, url_for
from flask_cors import CORS
import json
import models as m
import utils as u
import config as c
import time
import random

app = Flask(__name__)
CORS(app)

user_id = 0
users = {}

coords = {
    "x": 0,
    "y": 0,
}

@app.route('/up')
def up():
    coords["y"] -= 1
    return json.dumps(coords)

@app.route('/down')
def down():
    coords["y"] += 1
    return json.dumps(coords)

@app.route('/left')
def left():
    coords["x"] -= 1
    return json.dumps(coords)

@app.route('/right')
def right():
    coords["x"] += 1
    return json.dumps(coords)

@app.route('/ok')
def ok():
    rnd = ['+', '-']
    return random.choice(rnd)

a = 0

@app.route('/register', methods=['GET', 'POST'])
def register():
    print(a)
    if request.method == 'POST':
        js_dict = json.loads(request.data.decode('UTF-8'))
        users[user_id] = m.User(user_id, js_dict['login'], js_dict['password'], 0, 0)
        print(users)
        user_id += 1
    return 'ok'




